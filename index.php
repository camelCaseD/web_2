<?php ob_start(); session_start();?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="cloud,computing,ubuntu,one,microsoft,azure,amazon,EC2,google,chrome,OS,os,cr-48,notebook,sisters,middle,school,oregon,student,web,pages" />
<link rel="stylesheet" type="text/css" href="main.css" />
<script type="text/javascript">
function show_written_date() {
alert('The following page was written on 2.16.11. So the info on may not be up to date.');
}
function redirect(elem) {
switch (elem) {

case 'u':
var url = 'u.php';
break;

case 'c':
var url = 'c.php';
break;

case 'm':
var url = 'm.php';
break;

case 'p':
var url = 'p.php';
break;
}
window.location.href = url;
}

function changeTextColor(color1, id) {
 document.getElementById(id).style.color = color1;
}
</script>
<title>Cloud Computing</title>
</head>

<?php $npu = $_GET['npu']; if(isset($_SESSION['npu']) && isset($npu)) { if($_SESSION['npu']=='true' && $npu=='false') { echo"<body>\n";}} if(isset($npu)) { if($npu == 'true'){echo"<body>\n";} if($npu == 'false'){echo "<body onload=\"show_written_date()\">\n"; $_SESSION['npu'] = 'true';} }else{header("Location: index.php?npu=false");}?>
<table style="font-size:16pt;" align="center" border="0" width="95%">
	<tr>
		<td colspan="3">
		<table border="0" width="100%">
			<tr>
				<td>
				<p align="center"><img src="images/banner.jpg" /></p></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">
		<table border="0" width="100%">
			<tr>
				<td>
				<p align="center">
				<marquee style="font-size:16pt;">Cloud Computing</marquee></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3"><center><img src="images/service_providers.jpg" /></center></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
		<table border="0" width="100%">
			<tr>
				<td><span style="font-size: 16pt"><p style="text-indent: .5in;">Cloud computing is when you store all your files out on the web. Cloud computing is a new thing right now. Most people can't easily access it yet. The easiest way to access it at the time of this writing is through <a id="u" onMouseOut="changeTextColor('#CC7F32', 'u')" onMouseOver="changeTextColor('#FF5333', 'u')" onClick="redirect('u')">Ubuntu One</a> which uses Amazon's EC2 (Elastic Compute Cloud) servers. The next way is to be released by the end of 2011 which is Google <a id="c" onMouseOut="changeTextColor('#CC7F32', 'c')" onMouseOver="changeTextColor('#FF5333', 'c')" onClick="redirect('c')">Chrome OS</a>. Chrome OS is just like Google's Chrome web browser with all the apps you can install on it except it is only the web. So your computer only takes about 10 seconds to start up then login and you are good to go. Now if you are running a corporate company then you would want to use <a id="m" onMouseOut="changeTextColor('#CC7F32', 'm')" onMouseOver="changeTextColor('#FF5333', 'm')" onClick="redirect('m')">Microsoft Azure</a>. Which is why do not need to pay for the infrastructure just for how much data you need.</p></span></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
		<table border="0" width="100%">
			<tr>
				<td>
				<span style="font-size: 16pt;text-indent:.5in;">
				Cloud Computing is a new and amazing thing in the technology world. By the year 2012 everyone should be able to easily access it through one of the programs talked about on the different pages on this web page.</span></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<div id="footer"><p style="font-size:12pt;"><form name="search" action="#" method="post"><input id="q" type="text" name="q" value="Search... (Disabled)" />&nbsp;<input style="padding: 2px;" type="submit" value="Search" /><input type="hidden" name="w" value"s2" /></form><a id="p" onMouseOut="changeTextColor('#CC7F32', 'p')" onMouseOver="changeTextColor('#FF5333', 'p')" onclick="redirect('p')">Edit Pop-Ups</a></p></div>

<a id="source" onMouseOut="changeTextColor('#CC7F32', 'source')" onMouseOver="changeTextColor('#FF5333', 'source')" href="https://gitlab.com/camelCaseD/web_2" target="_blank">Source Code</a>
</body>

</html>
<?php ob_end_flush();?>
