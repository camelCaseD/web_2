<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="main.css" />
<title>Chrome OS</title>
<script type="text/javascript">
function show_confirm(n) {
switch(n) {
case 'u':
var na = 'Ubuntu One';
var url = 'one.ubuntu.com';
break;

case 'c':
var na = 'Chrome OS';
var url = 'google.com/chromeos/';
break;

case'm':
var na = 'Microsoft Azure';
var url = 'microsoft.com/windowsazure/';
break; 
}
var c=confirm("Click okay if you want to go to "+na+"'s website or click cancel to view more info on "+na+" from this page.");
if(c==true) {
window.location.href = "http://www."+url;
}
}
function redirect(elem) {
switch(elem) {
case'b':
var url = 'index.php?npu=true';
break;

case'c':
var url = 'http://www.google.com/chromeos/';
break;

case'p':
var url = 'p.php';
break;
}
window.location.href = url;
}

function changeTextColor(color1, id) {
 document.getElementById(id).style.color = color1;
}
function roll_over(img_name, img_src)
{
 document[img_name].src = img_src;
}
</script>
</head>

<?php
if(isset($_COOKIE['pu'])){
$pu = $_COOKIE['pu'];
if($pu=='npu'){
?>
<body>
<?php
}
if($pu=='pu'){
?>
<body onLoad="show_confirm('c')">
<?php
}
}
?>
<table border="0" width="95%">
	<tr>
		<td colspan="3">
		<table border="0" width="100%">
			<tr>
				<td>
				<p style="text-indent:.5in;font-size:16pt;">Chrome OS is made by Google. Chrome OS is only available right now to people who can enter the pilot program and get a Cr-48 chrome notebook. The features include it only takes about 10 seconds to start up then login to your Google account. The only thing you can access on it is the internet through the Google Chrome web browser. But with that you can also download web apps from the Google web market. So all your info is stored on Google’s custom servers not your computer.</p></td>
			</tr>
		</table>
		</td>
	</tr>

	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
		<table border="0" width="100%">
			<tr>
				<td>
				<p align="center"><img name="pic" onmouseover="roll_over('pic', 'images/cr48-1.jpg')" onmouseout="roll_over('pic', 'images/chrome_logo_transparent.png')" src="images/chrome_logo_transparent.png" style="cursor: pointer;" alt="Cr48 Notebook" onclick="redirect('c')" /></p>
				<p align="center"><span style="font-size:14pt;">Cr-48 Notebook</span></p></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
		<table border="0" width="95%">
			<tr>
				<td><a id="b" onMouseOut="changeTextColor('#CC7F32', 'b')" onMouseOver="changeTextColor('#FF5333', 'b')" onClick="redirect('b')">
				Back</a></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<div id="footer"><p style="font-size:12pt;"><form name="search" action="#" method="post"><input id="q" type="text" name="q" value="Search... (Disabled)" />&nbsp;<input style="padding: 2px;" type="submit" value="Search" /><input type="hidden" name="w" value"s2" /></form><a id="p" onMouseOut="changeTextColor('#CC7F32', 'p')" onMouseOver="changeTextColor('#FF5333', 'p')" onclick="redirect('p')">Edit Pop-Ups</a></p></div>

<a id="source" onMouseOut="changeTextColor('#CC7F32', 'source')" onMouseOver="changeTextColor('#FF5333', 'source')" href="https://gitlab.com/camelCaseD/web_2" target="_blank">Source Code</a>
</body>

</html>
