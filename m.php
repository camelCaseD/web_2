<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="main.css" />
<title>Microsoft Azure</title>
<script type="text/javascript">
function show_confirm(n) {
switch(n) {
case 'u':
var na = 'Ubuntu One';
var url = 'one.ubuntu.com';
break;

case 'c':
var na = 'Chrome OS';
var url = 'google.com/chromeos/';
break;

case'm':
var na = 'Microsoft Azure';
var url = 'microsoft.com/windowsazure/';
break; 
}
var c=confirm("Click okay if you want to go to "+na+"'s website or click cancel to view more info on "+na+" from this page.");
if(c==true) {
window.location.href = "http://www."+url;
}
}
function redirect(elem) {
switch(elem) {
case'b':
var url = 'index.php?npu=true';
break;

case'm':
var url = 'http://www.microsoft.com/windowsazure/';
break;

case'p':
var url = 'p.php';
break;
}
window.location.href = url;
}

function changeTextColor(color1, id) {
 document.getElementById(id).style.color = color1;
}
</script>
</head>
<?php
if(isset($_COOKIE['pu'])){
$pu = $_COOKIE['pu'];
if($pu=='npu'){
?>
<body>
<?php
}
if($pu=='pu'){
?>
<body onLoad="show_confirm('m')">
<?php
}
}
?>
<table border="0" width="95%">
	<tr>
		<td colspan="3">
		<table border="0" width="100%">
			<tr>
				<td>
				<span style="font-size: 16pt; text-indent:.5in;">
				Microsoft Azure is a version of cloud computing meant for corporate companies. Azure was designed for that audience. The only thing your company has to pay for is how much data they use (5 cents per hour). All the info that your company uses is stored on Microsoft’s servers.</span></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
		<table border="0" width="100%">
			<tr>
				<td>
				<p align="center"><img src="images/windows_azure_logo.jpg" style="cursor: pointer;" alt="Windows Azure Logo" onclick="redirect('m')" /></p>
				<p align="center"><span style="font-size:14pt;">Windows Azure Logo</span></p></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
		<table border="0" width="100%">
			<tr>
				<td><a id="b" onMouseOut="changeTextColor('#CC7F32', 'b')" onMouseOver="changeTextColor('#FF5333', 'b')" onClick="redirect('b')">
		Back</a></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<div id="footer"><p style="font-size:12pt;"><form name="search" action="#" method="post"><input id="q" type="text" name="q" value="Search... (Disabled)" />&nbsp;<input style="padding: 2px;" type="submit" value="Search" /><input type="hidden" name="w" value"s2" /></form><a id="p" onMouseOut="changeTextColor('#CC7F32', 'p')" onMouseOver="changeTextColor('#FF5333', 'p')" onclick="redirect('p')">Edit Pop-Ups</a></p></div>

<a id="source" onMouseOut="changeTextColor('#CC7F32', 'source')" onMouseOver="changeTextColor('#FF5333', 'source')" href="https://gitlab.com/camelCaseD/web_2" target="_blank">Source Code</a>
</body>

</html>
